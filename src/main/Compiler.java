package main;

import compiler.*;
import parser.ParseException;
import parser.Parser;
import types.IType;
import util.CompilerEnv;
import util.DuplicateIdentifierException;
import util.Environment;
import util.StaticTypingException;
import util.UndeclaredIdentifierException;

import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;

import ast.ASTNode;

public class Compiler {

  public static void main(String args[]) {
    Parser parser = new Parser(System.in);
    ASTNode exp;

    try {
      exp = parser.Start();

      CodeBlock code = new CodeBlock();
      exp.compile(code, null);

      code.dump("Demo.j");

    } catch (Exception e) {
      System.out.println ("Syntax Error!");
    }
  }
  
	public static void compile(String s) throws ParseException, FileNotFoundException, UndeclaredIdentifierException, DuplicateIdentifierException, StaticTypingException {
		Parser parser = new Parser(new ByteArrayInputStream(s.getBytes()));
		ASTNode n = parser.Start();
		
		IType type = n.typeCheck(new Environment<IType>());
        CodeBlock code = new CodeBlock();
		n.compile(code, new CompilerEnv());
		
		code.dump("Demo.j");
	}

}
