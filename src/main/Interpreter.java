package main;
import java.io.ByteArrayInputStream;

import ast.ASTNode;
import parser.*;
import types.IType;
import util.DuplicateIdentifierException;
import util.Environment;
import util.ExecutionErrorException;
import util.StaticTypingException;
import util.DynamicTypingException;
import util.UndeclaredIdentifierException;
import values.IValue;

public class Interpreter {
	
	public static IValue evaluate(String s) throws ParseException, UndeclaredIdentifierException, DuplicateIdentifierException, ExecutionErrorException, StaticTypingException, DynamicTypingException {
		Parser parser = new Parser(new ByteArrayInputStream(s.getBytes()));
		ASTNode exp = parser.Start();
//		exp.typeCheck(new Environment<IType>());
		return exp.eval(new Environment<IValue>());
	}

	public static void main(String args[]) {
		Parser parser = new Parser(System.in);
	    ASTNode exp;
	
	    while (true) {
		    try {
		      exp = parser.Start();
//		      IType type = exp.typeCheck(new Environment<IType>());
//		      System.out.println( type.toString() );
		      System.out.println( exp.toString() + " = "+ exp.eval(new Environment<IValue>()) );
		    } catch (ParseException e) {
		    	System.out.println ("Syntax Error!");
		    	e.printStackTrace();
		    	parser.ReInit(System.in);
		    } catch (UndeclaredIdentifierException e) {
		    	System.out.println("Undeclared Identifier "+e.getId());
				parser.ReInit(System.in);
			} catch (DuplicateIdentifierException e) {
				System.out.println("Duplicate Identifier "+e.getId());
				parser.ReInit(System.in);
			} catch (DynamicTypingException e) {
				System.out.println("Type Error: "+e.getMessage());
				parser.ReInit(System.in);
			} catch (ExecutionErrorException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
	    }
	}
}
