package tests;

import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

import org.junit.Test;

import main.Compiler;
import parser.ParseException;
import util.DuplicateIdentifierException;
import util.StaticTypingException;
import util.UndeclaredIdentifierException;

public class CompilerTests {

	// This test function was designed to work with Unix like systems. 
	
	private void testCase(String expression, String result) 
			throws IOException, InterruptedException, ParseException, FileNotFoundException, UndeclaredIdentifierException, DuplicateIdentifierException {
		
		Process p;
		
		p = Runtime.getRuntime().exec(new String[]{"sh", "-c", "rm *.j *.class"});
	    p.waitFor();	    

	    System.out.println("Compiling to Jasmin source code");

	    try {
			Compiler.compile(expression);
		} catch (StaticTypingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	    System.out.println("Compiling to Jasmin bytecode");
	    
		p = Runtime.getRuntime().exec(new String[]{"sh", "-c", "java -jar jasmin.jar *.j"});
	    p.waitFor();	    
	    assertTrue("Compiled to Jasmin bytecode", p.exitValue() == 0);

	    BufferedReader reader = 
		         new BufferedReader(new InputStreamReader(p.getInputStream()));

	    StringBuffer output = new StringBuffer();
        String line = "";			
        while ((line = reader.readLine())!= null) {
        		output.append(line + "\n");
        }
	    System.out.println(output.toString());

		p = Runtime.getRuntime().exec(new String[] {"sh","-c", "java Demo"});
	    p.waitFor();

	    reader = new BufferedReader(new InputStreamReader(p.getInputStream()));

	    output = new StringBuffer();
        line = "";			
        while ((line = reader.readLine())!= null) {
        		output.append(line + "\n");
        }
	    System.out.println("Output: #"+output.toString()+"#");
	    
	    assertTrue(result.equals(output.toString()));
	}
	
	private void testCase(String expression, int value) throws FileNotFoundException, IOException, InterruptedException, ParseException, UndeclaredIdentifierException, DuplicateIdentifierException {
		testCase(expression, value+"\n");		
	}

	@Test
	public void BasicTest() throws IOException, InterruptedException, ParseException, UndeclaredIdentifierException, DuplicateIdentifierException{
		testCase("1\n", "1\n");
	}

	@Test
	public void testsLabClass02() throws Exception {
		testCase("1+2\n",3);
		testCase("1-2-3\n",-4);
		testCase("4*2\n",8);
		testCase("4/2/2\n",1);
	}
	
	@Test
	public void testDeclarations() throws IOException, InterruptedException, ParseException, UndeclaredIdentifierException, DuplicateIdentifierException {
		testCase("decl x = 1 y = 2*3 z = 4*5 in x+y*z end\n", "121\n");
	}
	
	@Test
	public void testDeclarations0() throws IOException, InterruptedException, ParseException, UndeclaredIdentifierException, DuplicateIdentifierException {
		testCase("decl x = 1 in x end + decl y = 2 in y end\n", "3\n");
	}
	
	@Test
	public void testDeclarations1() throws IOException, InterruptedException, ParseException, UndeclaredIdentifierException, DuplicateIdentifierException {
		testCase("decl x = 1 in decl y = 2 in  x+y  end end\n", "3\n");
	}
	
	@Test
	public void testDeclarations2() throws IOException, InterruptedException, ParseException, UndeclaredIdentifierException, DuplicateIdentifierException {
		testCase("decl x = 1 in decl y = 2 in decl z = 3 in x+y+z end end end\n", "6\n");
	}
	
	@Test
	public void testDeclarations3() throws IOException, InterruptedException, ParseException, UndeclaredIdentifierException, DuplicateIdentifierException {
		testCase("decl a = 2 in decl x = 1 in x + 2 end + decl y = 2 in y + 2 end end\n", "7\n");
	}
	
	@Test
	public void testVariables1() throws IOException, InterruptedException, ParseException, UndeclaredIdentifierException, DuplicateIdentifierException {
		testCase("decl x = var(var(1)) in *x := **x + 1; **x end\n", "2\n");
	}
	
	@Test
	public void testIfThemElse() throws IOException, InterruptedException, 
											ParseException, UndeclaredIdentifierException, 
											DuplicateIdentifierException {
		testCase("if true then 1+1 else 2+2 end\n", "2\n");
	}
	
	@Test
	public void testWhile() throws IOException, InterruptedException, 
											ParseException, UndeclaredIdentifierException, 
											DuplicateIdentifierException {
		testCase("while false do 1+1 end; 1+1\n", "2\n");
	}
	
    @Test
    public void testVariables5() throws Exception {
    	testCase("*var(2)\n", "2\n");
    }
    
    @Test
    public void testVariables6() throws Exception {
    	testCase("**var(var(2))\n", "2\n");
    }
	@Test
	public void testVariables() throws IOException, InterruptedException, ParseException, UndeclaredIdentifierException, DuplicateIdentifierException {
		testCase("decl x = var(1) in  x:=3 + *x; 2 + *x end\n", "6\n");
	}
	@Test
	public void testVariables2() throws IOException, InterruptedException, ParseException, UndeclaredIdentifierException, DuplicateIdentifierException {
		testCase("decl x = 1 in 2 * x end\n", "2\n");
	}
	
	@Test
	public void testVariables3() throws IOException, InterruptedException, ParseException, UndeclaredIdentifierException, DuplicateIdentifierException {
		testCase("decl x = var(0) in decl y = x := *x + 1 in 2 * *x end end\n", "2\n");
	}
	@Test
	public void testVariables4() throws IOException, InterruptedException, ParseException, UndeclaredIdentifierException, DuplicateIdentifierException {
		testCase("decl x = var(1) y = var(2) in *y * *x end\n", "2\n");
	}

    @Test
    public void testVariables7() throws Exception {
    	testCase("decl x = var(1) in decl y = var(x) in x := **y+2 end end\n", "3\n");
    }
    @Test
    public void testVariables8() throws Exception {
    	testCase("decl x = 1 in decl y = 1 + x in y+x end end\n", "3\n");
    }
}
