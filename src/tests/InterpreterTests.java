package tests;

import static org.junit.Assert.*;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.junit.Test;

import main.*;
import parser.ParseException;
import values.IntValue;
import util.DuplicateIdentifierException;
import util.DynamicTypingException;
import util.UndeclaredIdentifierException;

public class InterpreterTests {

	private void testCase(String expression, int value) throws ParseException {
		assertTrue(Console.acceptCompare(expression,value));		
	}
	
	private void testNegativeCase(String expression, int value) throws ParseException {
		assertFalse(Console.acceptCompare(expression,value));
	}
	
	@Test
	public void test01() throws Exception {
		testCase("1\n",1);
	}
	
	@Test
	public void testsLabClass02() throws Exception {
		testCase("1+2\n",3);
		testCase("1-2-3\n",-4);
		testCase("4*2\n",8);
		testCase("4/2/2\n",1);
	}
	
	@Test(expected = UndeclaredIdentifierException.class)
	public void testDeclarations1() throws Exception {
		Interpreter.evaluate("x\n");
	}

	@Test(expected = UndeclaredIdentifierException.class)
	public void testDeclarations2() throws Exception {
		Interpreter.evaluate("decl x = 1 in y end\n");
	}
	
	@Test(expected = UndeclaredIdentifierException.class)
	public void testDeclarations3() throws Exception {
		Interpreter.evaluate("decl x = x in x+1 end\n");
	}
	
	@Test(expected = UndeclaredIdentifierException.class)
	public void testDeclarations4() throws Exception {
		Interpreter.evaluate("decl x = 1 y = x in x+1 end\n");
	}

	@Test(expected = DuplicateIdentifierException.class)
	public void testDeclarations5() throws Exception {
		Interpreter.evaluate("decl x = 1 x = 3 in x+1 end\n");
	}
	
	@Test
	public void testsLabClass04() throws Exception {
		testCase("decl x = decl y = 3 in 10*y end in x + decl y = 2 in x+y end end\n",62);
		testCase("decl x = 1 in decl x = 2 in x*5 end + x end\n", 11);
		testCase("decl x = 1 y = 2 in decl x = 3 z = 4 in x + y + z end end\n",9);
	}
	
	@Test
	public void testsLabClass05() throws Exception {
		testCase("decl x=1 in x+2 end\n",3);
		testCase("decl x=1 in x+decl x=2 in x+1 end end\n",4);
		testCase("decl x=1 in x+decl y=2 in x+y end end\n",4);
		testCase("decl x=1 y=2 in x+y end\n",3);
	}
	
	@Test
	public void testVariables() throws Exception {
		testCase("decl x = var(0) in x := *x + 1 ; 2 * *x end\n",2);
	}
	
	@Test
	public void test10Function() throws Exception {
		testCase("decl f = fun x : int, y : int => x + y end in f(1,1) end\n",2);		
	}
	
	@Test
	public void test11FunctionAsArgument() throws Exception {
        testCase("decl x = decl y = 3 in 10*y end in x + decl y = 2 in x+y end end\n",62);
        testCase("decl x = 1 in decl x = 2 in x*5 end + x end\n",11);
        testCase("decl x = 1 y = 2 in decl x = 3 z = 4 in x + y + z end end\n",9);
        testCase("decl x = var(1) in x := 2; *x + 1 end\n", 3);
        testCase("decl x = var(1) in *x end\n", 1);
        testCase("decl x = var(1) in *x + 3 end\n",4);
        testCase("decl x = var(1)  in while *x < 10 do x := *x + 1 end ; *x end\n", 10);
        testCase("decl x = var(1)  in while *x < 10 do x := *x + 1 end ; *x - 5 end\n",5);
        testCase("decl x = var(1)  in while *x < 10 do x := *x + 1 end ; *x - 5 ; *x end\n", 10);
        testCase("decl x = var(1)  in if *x == 1 then while *x < 10 do x := *x + 1 end else 10 end ; *x end\n", 10);
        testCase("decl x = var(1)  in if *x == 1 then true else while *x < 10 do x := *x + 1 end end ; *x end\n",1);
        testCase("decl x = var(1) in decl y=x in decl w=y in w:= *w+*y;*x end end end\n", 2);	
        testCase("decl r1 = var(5) r2 = var(3) a = 10 b = 20 c = 6 in fun x:(ref(int), ref(int))->int, y:int => r1:=(a / *r1); x(r1, r2) * y end (fun i:ref(int), j:ref(int) => *i + *j + b end, c) end\n", 150);
	}
	
    @Test
    public void testIfThenElseandSeqwithRefForRefInt() throws Exception {
    	testCase("decl x = var(1) in decl y = var(x)  in x:=**y+2 end end\n", 3);
    }
	
	@Test

	public void testFibonacci() throws Exception {
		testCase("declrec fib = fun x:int => if x == 0 then 0 else if x == 1 then 1 else fib(x-1) + fib(x-2) end end end in fib(20) end\n", 6765);
//	    testCase("declrec dis = {a = fun x:int => if x == 0 then 0 else if x == 1 then 1 else dis.a(x-1)+dis.a(x-2) end end end} in dis.a(20) end\n", 6765);
//	    testCase("decl a = {{a = fun x:int => if x == 0 then 0 else if x == 1 then 1 else this.a(x-1)+this.a(x-2) end end end}} in a.a(20) end\n", 6765);
	}
}
