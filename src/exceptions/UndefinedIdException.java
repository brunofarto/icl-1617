package exceptions;

public class UndefinedIdException extends Exception{
	private static final long serialVersionUID = -6243514624431189666L;

	public UndefinedIdException() {
		super();
	}

	public UndefinedIdException(String message) {
		super(message);
	}
	

}
