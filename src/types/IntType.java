package types;

public class IntType implements IType {
	public static final IntType singleton = new IntType();

	private IntType(){}	
	public String toString(){return "int";}	
}
