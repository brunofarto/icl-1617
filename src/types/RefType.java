package types;

public class RefType implements IType {
	private final IType type;
	
	public RefType(IType type){
		this.type = type;
	}
	
	public String toString(){
		return "ref("+type+")";
	}
	
	public IType getType(){
		return type;
	}

//	public boolean equals(Object other){
//		if(other instanceof RefType){
//			IType type = ((RefType) other).getType();
//			return this.type.equals(type);
//		} else 
//			return false;
//	}
}
