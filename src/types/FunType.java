package types;

import java.util.ArrayList;

import util.TypedBinding;

public class FunType implements IType {
	private ArrayList<IType> argsType;
	private IType returnType;
		
	public FunType(ArrayList<IType> argsType,IType returnType){
		this.argsType = argsType;
		this.returnType = returnType;
	}
		
	public ArrayList<IType> getArgsType(){ return argsType;}
	public IType getReturnType(){ return returnType;}
	
//	public boolean equals(Object returnType){
//		if(other instanceof FunType){
//			IType type = ((FunType) other).getType();
//			return this.parType.equals(type);
//		} else 
//			return false;
//	}

	private IType getType() {
		return returnType;
	}
	
	public String toString(){
		String res = "(";
//		TypedBinding last = arguments.get( arguments.size());
		int i = 0;
		for(IType arg:argsType){
			res += arg.toString();
			if (i<argsType.size()-1){
				res += ",";
			}
			i++;
		}
		return res + ") -> " + returnType.toString();
	}
}
