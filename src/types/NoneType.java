package types;

public class NoneType implements IType {
	public static final NoneType singleton = new NoneType();

	private NoneType(){}	
	public String toString(){return "noneType";}	
}
