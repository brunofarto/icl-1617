package util;
import java.util.*;

import types.IType;

public class CompilerEnv<T> {
	static class Assoc<T> {
		String id;
		T value;
		
		Assoc(String id, T value) {
			this.id = id;
			this.value = value;
		}
	}
	
	public class Address{
		public int jumps;
		public T val;
		
		public Address(int jumps, T val){
			this.jumps = jumps;
			this.val = val;
		}
	}

	CompilerEnv<T> up;
	ArrayList<Assoc<T>> assocs;
	
	public CompilerEnv() {
		this.up = null;
		this.assocs = new ArrayList<Assoc<T>>();
	}
	
	private CompilerEnv(CompilerEnv<T> up) {
		this();
		this.up = up;
	}

	public Address lookup(String id) throws UndeclaredIdentifierException {
		int jumps = 0;
		
		CompilerEnv<T> current = this;
		while(current != null) {
			for(Assoc<T> assoc: current.assocs)
				if( assoc.id.equals(id))
					return new Address(jumps,assoc.value);
			current = current.up;
			jumps++;
		}
		throw new UndeclaredIdentifierException(id);
	}

	public CompilerEnv<T> beginScope() {
		return new CompilerEnv<T>(this);
	}
	
	public CompilerEnv<T> endScope() {
		return up;
	}
	
	public int addSlot(String id) throws DuplicateIdentifierException {
		for(Assoc assoc: assocs)
			if(assoc.id.equals(id))
				throw new DuplicateIdentifierException(id);
		int value = assocs.size();
		
		assocs.add(new Assoc(id,value));

		return value;
	}
}
