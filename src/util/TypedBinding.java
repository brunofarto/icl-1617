package util;
import types.IType;

public class TypedBinding {
	private String id;
	private IType type;
	
	public TypedBinding(String id, IType type){
		this.id = id;
		this.type = type;
	}
	
	public String getId(){
		return id;
	}
	
	public IType getType(){
		return type;
	}
}
