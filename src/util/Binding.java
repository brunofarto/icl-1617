package util;
import ast.ASTNode;
import types.IType;

public class Binding {
	private String id;
	private ASTNode expr;
	private IType type;

	public Binding(String id, ASTNode expr) {
		this.id = id;
		this.expr = expr;
	}

	public String getId() {
		return this.id;
	}

	public ASTNode getExpr() {
		return expr;
	}
	
	public IType getType() {
		return type;
	}

	public void setType(IType type) {
		this.type = type;
	}
}
