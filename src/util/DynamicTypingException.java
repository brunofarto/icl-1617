package util;

public class DynamicTypingException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4913432163879740107L;

	public DynamicTypingException(String message){
		super(message);
	}
}
