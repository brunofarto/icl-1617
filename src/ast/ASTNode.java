package ast;

import compiler.CodeBlock;
import types.IType;
import util.CompilerEnv;
import util.DuplicateIdentifierException;
import util.DynamicTypingException;
import util.Environment;
import util.ExecutionErrorException;
import util.StaticTypingException;
import util.UndeclaredIdentifierException;
import values.IValue;

public interface ASTNode {

	IValue eval(Environment<IValue> env) throws DynamicTypingException, UndeclaredIdentifierException, DuplicateIdentifierException, ExecutionErrorException;

	IType typeCheck(Environment<IType> env) throws UndeclaredIdentifierException, DuplicateIdentifierException, StaticTypingException;

	void compile(CodeBlock code,CompilerEnv env) throws UndeclaredIdentifierException, DuplicateIdentifierException;

}

