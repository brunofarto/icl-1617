package ast;

import compiler.CodeBlock;
import types.BoolType;
import types.IType;
import types.IntType;
import util.CompilerEnv;
import util.DuplicateIdentifierException;
import util.Environment;
import util.StaticTypingException;
import util.UndeclaredIdentifierException;
import values.BoolValue;
import values.IValue;

public class ASTBool implements ASTNode{
	Boolean val;
	
    public IValue eval(Environment<IValue> env) { 
    	return new BoolValue(val); 
    }

	public ASTBool(Boolean n) {
		val = n;
	}
	
	public IType getType() { return BoolType.singleton; }

	@Override
	public String toString() {
		return Boolean.toString(val);
	}

	public IType typeCheck(Environment<IType> env)
			throws UndeclaredIdentifierException, DuplicateIdentifierException, StaticTypingException {

		return BoolType.singleton;
	}
	
	public void compile(CodeBlock code,CompilerEnv env) {
		code.emit_push(val?1:0);
	}
}
