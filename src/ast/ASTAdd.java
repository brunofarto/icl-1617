package ast;

import compiler.CodeBlock;
import types.IType;
import types.IntType;
import types.NoneType;
import util.CompilerEnv;
import util.DuplicateIdentifierException;
import util.DynamicTypingException;
import util.Environment;
import util.ExecutionErrorException;
import util.StaticTypingException;
import util.UndeclaredIdentifierException;
import values.IValue;
import values.IntValue;

public class ASTAdd implements ASTNode {

	ASTNode left, right;

	public ASTAdd(ASTNode l, ASTNode r) {
		left = l;
		right = r;
	}

	public IValue eval(Environment<IValue> env) throws UndeclaredIdentifierException, DuplicateIdentifierException, ExecutionErrorException, DynamicTypingException {
		IValue l = left.eval(env);
		IValue r = right.eval(env);
		if((l instanceof IntValue && r instanceof IntValue) ){
			return new IntValue(((IntValue)l).getValue() + ((IntValue)r).getValue());
		} throw new DynamicTypingException(null);
	}

	public void compile(CodeBlock code, CompilerEnv env) throws UndeclaredIdentifierException, DuplicateIdentifierException {
		left.compile(code, env);
		right.compile(code, env);
		code.emit_add();
	}

	public String toString() {
		return left.toString() + " + " + right.toString();
	}

	@Override
	public IType typeCheck(Environment<IType> env)
			throws UndeclaredIdentifierException, DuplicateIdentifierException, StaticTypingException {
		IType t1=left.typeCheck(env);
		IType t2=right.typeCheck(env);
		if(t1== NoneType.singleton || t2== NoneType.singleton )
			return IntType.singleton;
		if(t1==IntType.singleton&&t2==IntType.singleton)
			return IntType.singleton;
		else
			throw new StaticTypingException("Types dont match!");
	}
}
