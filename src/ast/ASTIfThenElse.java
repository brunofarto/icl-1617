package ast;

import compiler.CodeBlock;
import types.IType;
import util.CompilerEnv;
import util.DuplicateIdentifierException;
import util.DynamicTypingException;
import util.Environment;
import util.ExecutionErrorException;
import util.StaticTypingException;
import util.UndeclaredIdentifierException;
import values.BoolValue;
import values.IValue;

public class ASTIfThenElse implements ASTNode {
    private ASTNode c,e1,e2;

    public ASTIfThenElse(ASTNode condition,ASTNode exp1,ASTNode exp2){
        this.c=condition;
        this.e1=exp1;
        this.e2=exp2;
    }

	public IValue eval(Environment<IValue> env) throws DynamicTypingException, UndeclaredIdentifierException,
			DuplicateIdentifierException, ExecutionErrorException {
    	BoolValue cond = (BoolValue)c.eval(env);
		if(cond.getValue())
			return e1.eval(env);
		else
			return e2.eval(env); 
	}
	
    public String toString() {
        return "if "+c.toString()+" then "+e1.toString()+" else "+e2.toString()+" end";
    }

	@Override
	public IType typeCheck(Environment<IType> env)
			throws UndeclaredIdentifierException, DuplicateIdentifierException, StaticTypingException {
		IType type = e1.typeCheck(env);

		if(type.equals(e2.typeCheck(env))){
			return type;
		} else
			throw new StaticTypingException("Types dont match!");
	}

	@Override
	public void compile(CodeBlock code, CompilerEnv env)
			throws UndeclaredIdentifierException, DuplicateIdentifierException {
		c.compile(code, env);
		code.emitIf(e1,e2,code, env);
	}

}
