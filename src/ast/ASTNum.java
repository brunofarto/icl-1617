package ast;

import compiler.CodeBlock;
import types.IType;
import types.IntType;
import util.CompilerEnv;
import util.DuplicateIdentifierException;
import util.Environment;
import util.StaticTypingException;
import util.UndeclaredIdentifierException;
import values.IValue;
import values.IntValue;

public class ASTNum implements ASTNode {

	int val;
	
    public IValue eval(Environment<IValue> env){ 
    	return new IntValue(val); 
    }
    
	public IType typeCheck(Environment<IType> env)
			throws UndeclaredIdentifierException, DuplicateIdentifierException, StaticTypingException {
//		return IntType.value;
		return IntType.singleton;
	}
	
	public IType getType() { return IntType.singleton; }

	public ASTNum(int n) {
		val = n;
	}

	public void compile(CodeBlock code, CompilerEnv env) {
		code.emit_push(val);
	}

	public String toString() {
		return Integer.toString(val);
	}
}
