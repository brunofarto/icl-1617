package ast;

import compiler.CodeBlock;
import types.IType;
import types.RefType;
import util.CompilerEnv;
import util.DuplicateIdentifierException;
import util.DynamicTypingException;
import util.Environment;
import util.ExecutionErrorException;
import util.StaticTypingException;
import util.UndeclaredIdentifierException;
import values.IValue;
import values.RefValue;

public class ASTDeref implements ASTNode {
	
    ASTNode expr;
    IType type;
    
    public ASTDeref(ASTNode expr){
        this.expr = expr;       
    }
    
	@Override
	public IValue eval(Environment<IValue> env) throws DynamicTypingException, UndeclaredIdentifierException,
			DuplicateIdentifierException, ExecutionErrorException {
		RefValue ref =(RefValue) expr.eval(env);
		return ref.getValue();
	}

	@Override
	public IType typeCheck(Environment<IType> env)
			throws UndeclaredIdentifierException, DuplicateIdentifierException, StaticTypingException {
		IType ref = expr.typeCheck(env);
		
		if(ref instanceof RefType){
			this.type = ((RefType) ref).getType();
			return type;
		} else 
			throw new StaticTypingException("Missmatch type error:");	
	}

	@Override
	public void compile(CodeBlock code, CompilerEnv env)
			throws UndeclaredIdentifierException, DuplicateIdentifierException {
    	code.comment(" Start deref");
    	expr.compile(code, env);
    	String t = code.type(type);
    	code.emitGetfield(code.refClassOfType(new RefType(type)),"value",t.equals("I")?t:t+";");
	}
	
    public String toString() {
    	return "*"+expr.toString();
    }
}
