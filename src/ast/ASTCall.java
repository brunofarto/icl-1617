package ast;

import java.util.ArrayList;

import compiler.CodeBlock;
import types.FunType;
import types.IType;
import types.NoneType;
import util.CompilerEnv;
import util.DuplicateIdentifierException;
import util.DynamicTypingException;
import util.Environment;
import util.ExecutionErrorException;
import util.StaticTypingException;
import util.TypedBinding;
import util.UndeclaredIdentifierException;
import values.Closure;
import values.IValue;

public class ASTCall implements ASTNode {
	ASTNode lExp;
	ArrayList<ASTNode> rExps;
	
    public ASTCall(ASTNode l, ArrayList<ASTNode> r)
    {
    	lExp = l; rExps = r;
    }
	@Override
	public IValue eval(Environment<IValue> env) throws DynamicTypingException, UndeclaredIdentifierException,
			DuplicateIdentifierException, ExecutionErrorException {		
		IValue result = null;	
		IValue c = lExp.eval(env);	
						
		if(c instanceof Closure){
			Closure closure = (Closure) c;
			ArrayList<TypedBinding> param = closure.getArguments();
			ASTNode body = closure.getExp();
			
			Environment<IValue> newenv = closure.getEnv();
			newenv = newenv.beginScope();
			int i = 0;
			
			for(ASTNode exp: rExps){
				IValue v = exp.eval(env);
				newenv.assoc(param.get(i).getId(), v);
				i++;
			}
			
			result = body.eval(newenv);
			
			newenv.endScope();
		} else 
			throw new ExecutionErrorException("Closure error in Call funtion!!");
		
		return result;
	}

	@Override
	public IType typeCheck(Environment<IType> env)
			throws UndeclaredIdentifierException, DuplicateIdentifierException, StaticTypingException {
		IType leftType = lExp.typeCheck(env);
//		System.out.println(leftType.toString());
		IType argType = null;
		
		if(leftType == NoneType.singleton)
			return NoneType.singleton;
		
		if(leftType instanceof FunType ){
			IType funType = (FunType) leftType;
			int i = 0;
			for(IType arg:((FunType)funType).getArgsType()){
				argType = rExps.get(i).typeCheck(env);
				System.out.println(arg+" & "+argType);
				if(!(arg.equals(argType)))
					throw new StaticTypingException("Types dont match!");

				i++;
			}
//			System.out.println(((FunType) funType).getReturnType().toString());
			return ((FunType) funType).getReturnType();
		}else
			throw new StaticTypingException("Types dont match!");
	}

	@Override
	public void compile(CodeBlock code, CompilerEnv env)
			throws UndeclaredIdentifierException, DuplicateIdentifierException {
		// TODO Auto-generated method stub

	}
	
	public String toString() {
		String res = lExp.toString() + " (";
		for(ASTNode exp:rExps){
			res += exp.toString();
			if (!rExps.get( rExps.size()-1).equals(exp)){
				res += ", ";
			}
		}
		return res + ")";
	}
}
