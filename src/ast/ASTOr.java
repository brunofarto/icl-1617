package ast;

import compiler.CodeBlock;
import types.BoolType;
import types.IType;
import util.CompilerEnv;
import util.DuplicateIdentifierException;
import util.DynamicTypingException;
import util.Environment;
import util.ExecutionErrorException;
import util.StaticTypingException;
import util.UndeclaredIdentifierException;
import values.BoolValue;
import values.IValue;

public class ASTOr implements ASTNode {

	ASTNode left, right;

	public ASTOr(ASTNode l, ASTNode r) {
		left = l;
		right = r;
	}
	
	public IValue eval(Environment<IValue> env) throws UndeclaredIdentifierException, DuplicateIdentifierException, ExecutionErrorException, DynamicTypingException {
		IValue l = (BoolValue) left.eval(env);
		IValue r = (BoolValue) right.eval(env);
		if((l instanceof BoolValue && r instanceof BoolValue) ){
			return new BoolValue(((BoolValue) l).getValue() || ((BoolValue) r).getValue());
		} throw new DynamicTypingException(null);
    }

	public String toString() {
		return left.toString() + " || " + right.toString();
	}

	public void compile(CodeBlock code, CompilerEnv env) throws UndeclaredIdentifierException, DuplicateIdentifierException {
		left.compile(code, env);
		right.compile(code, env);
		code.emit_or();
	}
	
	public IType typeCheck(Environment<IType> env)
			throws UndeclaredIdentifierException, DuplicateIdentifierException, StaticTypingException {
		IType t1 = left.typeCheck(env);
		IType t2 = right.typeCheck(env);
		if(t1==BoolType.singleton&&t2==BoolType.singleton)
			return BoolType.singleton;
		else
			throw new StaticTypingException("Types dont match!");
	}
}