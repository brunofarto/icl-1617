package ast;

import compiler.CodeBlock;
import compiler.CodeBlock.TypeFrame;
import types.IType;
import types.RefType;
import util.CompilerEnv;
import util.DuplicateIdentifierException;
import util.DynamicTypingException;
import util.Environment;
import util.ExecutionErrorException;
import util.StaticTypingException;
import util.UndeclaredIdentifierException;
import values.IValue;
import values.RefValue;

public class ASTVar implements ASTNode {
    private ASTNode expr;
    private IType type;

    public ASTVar(ASTNode expr){
        this.expr = expr;
    }
    
	@Override
	public IValue eval(Environment<IValue> env) throws DynamicTypingException, UndeclaredIdentifierException,
			DuplicateIdentifierException, ExecutionErrorException {
		return new RefValue(expr.eval(env));
	}

	@Override
	public IType typeCheck(Environment<IType> env)
			throws UndeclaredIdentifierException, DuplicateIdentifierException, StaticTypingException {
		IType t1 = expr.typeCheck(env);
		this.type = t1;
		return new RefType(t1);
	}

	@Override
	public void compile(CodeBlock code, CompilerEnv env)
			throws UndeclaredIdentifierException, DuplicateIdentifierException {
		RefType refType = new RefType(type);
		code.comment(" Start Var type:"+refType);
		
		TypeFrame typeFrame = code.createRefFrame(refType);	
		code.initTypeFrame(typeFrame);
		expr.compile(code, env);
		String t = code.type(type);
		code.emitPutfield(code.refClassOfType(refType),"value",t.equals("I")?t:t+";");
		
	}
	
    public String toString() {
        return "var("+expr.toString()+")";
    }

}
