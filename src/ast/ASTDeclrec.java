package ast;

import java.util.ArrayList;

import compiler.CodeBlock;
import compiler.CodeBlock.StackFrame;
import types.IType;
import types.NoneType;
import util.Binding;
import util.CompilerEnv;
import util.DuplicateIdentifierException;
import util.DynamicTypingException;
import util.Environment;
import util.ExecutionErrorException;
import util.StaticTypingException;
import util.UndeclaredIdentifierException;
import values.IValue;

public class ASTDeclrec implements ASTNode {
	ArrayList<Binding> decls;
	ASTNode expr;
	
    public ASTDeclrec(ArrayList<Binding> decls, ASTNode expr)
    {
		this.decls = decls; 
		this.expr = expr;
    }
	
	public IValue eval(Environment<IValue> env) throws UndeclaredIdentifierException, DuplicateIdentifierException, ExecutionErrorException, DynamicTypingException {
		IValue value;
		IValue idValue;

		Environment<IValue> newEnv = env.beginScope();
		
		for (Binding decl:decls){
			newEnv.assoc(decl.getId(),null);
        	idValue = decl.getExpr().eval(newEnv);
        	newEnv.update(decl.getId(),idValue);
        }
		value = expr.eval(newEnv);
		newEnv.endScope();
		return value;
	}
	
	public IType typeCheck(Environment<IType> env) throws UndeclaredIdentifierException, DuplicateIdentifierException, StaticTypingException {
		IType type;
		IType idType;
		
		Environment<IType> newEnv = env.beginScope();		

		for( Binding decl:decls) {
			newEnv.assoc(decl.getId(),NoneType.singleton);
			idType = decl.getExpr().typeCheck(newEnv);
//			idType = decl.getExpr().typeCheck(env);
			decl.setType(idType);
			System.out.println("id: "+decl.getId()+", type: "+idType);
			newEnv.update(decl.getId(), idType);
		}

		type = expr.typeCheck(newEnv);
		newEnv.endScope();
	
		return type;
//		return null;
	}
	
	public void compile(CodeBlock code, CompilerEnv env) throws DuplicateIdentifierException, UndeclaredIdentifierException {
//		// create frame
//		StackFrame nframe = code.createFrame(decls.size());
//		CompilerEnv newEnv = env.beginScope();
//		int offSet = 0;
//		
//		code.initFrame(nframe);
//		
//		for(Binding decl:decls){
//			String id = decl.getId();
//			offSet = newEnv.addSlot(decl.getId());
//			
//			code.emit_dup();
//
//			decl.getExpr().compile(code,env);
//
//			code.emit_storeStack(nframe,offSet);
//			offSet++;
//		}
//		
//		// begin scope (push SP)
//		code.emit_pushFrame(nframe);
//		// associate id to address
//		expr.compile(code,newEnv);
//		// end scope (pop SP)
//		code.emite_popFrame();
//		newEnv.endScope();
	}
	
    
    public String toString() {
    	String s = "";
    	for(Binding decl: decls)
    		s += decl.getId() + " = " + decl.getExpr().toString()+" ";
    	return "declrec " + s + " in " + expr.toString() + " end";
    }



}
