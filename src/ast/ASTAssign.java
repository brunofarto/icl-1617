package ast;

import compiler.CodeBlock;
import types.IType;
import types.RefType;
import util.CompilerEnv;
import util.DuplicateIdentifierException;
import util.DynamicTypingException;
import util.Environment;
import util.ExecutionErrorException;
import util.StaticTypingException;
import util.UndeclaredIdentifierException;
import values.IValue;
import values.RefValue;

public class ASTAssign implements ASTNode {
    ASTNode lValue,rValue;
    IType valueType;
    
	public ASTAssign(ASTNode left, ASTNode right){
        this.lValue=left;
        this.rValue=right;
	}

	@Override
	public IValue eval(Environment<IValue> env) throws DynamicTypingException, UndeclaredIdentifierException,
			DuplicateIdentifierException, ExecutionErrorException {
        RefValue ref = (RefValue) lValue.eval(env);
		IValue val = rValue.eval(env);
		ref.setVal(val);
		return val;
	}

	@Override
	public IType typeCheck(Environment<IType> env)
			throws UndeclaredIdentifierException, DuplicateIdentifierException, StaticTypingException {
		IType type = rValue.typeCheck(env);
		RefType reftype = (RefType) lValue.typeCheck(env);
		if(reftype.getType().equals(type)){
			this.valueType = type;
			return type;
		} else
			throw new StaticTypingException("Missmatch type error:");
	}

	@Override
	public void compile(CodeBlock code, CompilerEnv env) throws UndeclaredIdentifierException, DuplicateIdentifierException {
		RefType refType = new RefType(valueType);
		code.comment(" Start Assign: "+refType);
		lValue.compile(code, env);
		
		code.checkcast(code.refClassOfType(refType));
		code.dup();

		rValue.compile(code, env);

		code.emitPutfield(code.refClassOfType(refType),"value",code.type(valueType));
		
		code.checkcast(code.refClassOfType(refType));
		code.emitGetfield(code.refClassOfType(refType),"value",code.type(valueType));
	}
	
    public String toString() {
        return lValue.toString()+":="+rValue.toString();
    }

}
