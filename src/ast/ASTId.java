package ast;
import compiler.CodeBlock;
import compiler.CodeBlock.StackFrame;
import types.IType;
import util.CompilerEnv;
import util.CompilerEnv.Address;
import util.DuplicateIdentifierException;
import util.Environment;
import util.ExecutionErrorException;
import util.StaticTypingException;
import util.UndeclaredIdentifierException;
import values.IValue;

public class ASTId implements ASTNode {

	String id;
	private IType idType;

	public ASTId(String id)
	{
		this.id = id;
	}

	public IValue eval(Environment<IValue> env) 
			throws UndeclaredIdentifierException, ExecutionErrorException { 
		return env.find(id); 
	}

	@Override
	public String toString() {
		return id;
	}

	@Override
	public void compile(CodeBlock code, CompilerEnv env) throws UndeclaredIdentifierException {
		// needs an environment -> Add it to the compile method signature
		// env.find(id); -> returns a pair (jumps, offset)
		Address addr = env.lookup(id);
		StackFrame frame = code.getCurrentFrame();
		int fnumber = frame.getNumber();
		int jumps = addr.jumps;
		
		code.aload_0();
		code.checkcast();
		// crawls the static link for the number of jumps		
		while(jumps>0){
			code.getfield("frame_"+fnumber+"/SL Lframe_"+(--fnumber)+";");
			jumps--;
		}
		String type = code.type(idType);
		// get the value from the frame in the given offset
		code.getfield("frame_"+fnumber+"/loc_"+addr.val+" "+(type.equals("I")?type:type+";"));
	}

	@Override
	public IType typeCheck(Environment<IType> env)
			throws UndeclaredIdentifierException, DuplicateIdentifierException, StaticTypingException {
		this.idType = env.find(id);
		return this.idType;
	}
	
	
}

