package ast;

import compiler.CodeBlock;
import types.BoolType;
import types.IType;
import util.CompilerEnv;
import util.DuplicateIdentifierException;
import util.DynamicTypingException;
import util.Environment;
import util.ExecutionErrorException;
import util.StaticTypingException;
import util.UndeclaredIdentifierException;
import values.BoolValue;
import values.IValue;

public class ASTWhile implements ASTNode {
    ASTNode c,e;

    public ASTWhile(ASTNode condition,ASTNode exp){
        this.c=condition;
        this.e=exp;
    }
    
	public IValue eval(Environment<IValue> env) throws DynamicTypingException, UndeclaredIdentifierException,
			DuplicateIdentifierException, ExecutionErrorException {
    	BoolValue cond = (BoolValue)c.eval(env);
		while(cond.getValue()){
			e.eval(env);
			cond = (BoolValue)c.eval(env);
		}
		return cond; 
	}

    public String toString() {
        return "while "+c.toString()+" do "+e.toString()+" end ";
    }
	
	public IType typeCheck(Environment<IType> env)
			throws UndeclaredIdentifierException, DuplicateIdentifierException, StaticTypingException {
		e.typeCheck(env);

		if(c.typeCheck(env)==BoolType.singleton){
			return BoolType.singleton;
		} else
			throw new StaticTypingException("Types dont match!");
	}

	@Override
	public void compile(CodeBlock code, CompilerEnv env)
			throws UndeclaredIdentifierException, DuplicateIdentifierException {
		code.emitWhile(c,e,code,env);
	}

}
