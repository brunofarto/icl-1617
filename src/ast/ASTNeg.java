package ast;

import compiler.CodeBlock;
import types.IType;
import types.IntType;
import util.CompilerEnv;
import util.DuplicateIdentifierException;
import util.DynamicTypingException;
import util.Environment;
import util.ExecutionErrorException;
import util.StaticTypingException;
import util.UndeclaredIdentifierException;
import values.IValue;
import values.IntValue;

public class ASTNeg implements ASTNode{
	ASTNode exp;
	
	public ASTNeg(ASTNode exp){
		this.exp = exp;
	}
	
	@Override
	public IValue eval(Environment<IValue> env) throws DynamicTypingException, UndeclaredIdentifierException,
			DuplicateIdentifierException, ExecutionErrorException {
		IValue e = exp.eval(env);
		if((e instanceof IntValue) ){
			return new IntValue(((IntValue)e).getValue()*(-1));
		} throw new DynamicTypingException(null);
	}

	public IType typeCheck(Environment<IType> env)
			throws UndeclaredIdentifierException, DuplicateIdentifierException, StaticTypingException {
		IType type = exp.typeCheck(env);
		if(type==IntType.singleton)
			return IntType.singleton;
		else
			throw new StaticTypingException("Types dont match!");
	}

	public void compile(CodeBlock code, CompilerEnv env) throws UndeclaredIdentifierException, DuplicateIdentifierException {
    	exp.compile(code,env);
        code.emit_push(-1);
        code.emit_mul();
	}
	
    public String toString() {
    	return "- " + exp.toString() ;
    }
}
