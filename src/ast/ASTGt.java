package ast;

import compiler.CodeBlock;
import types.BoolType;
import types.IType;
import types.IntType;
import util.CompilerEnv;
import util.DuplicateIdentifierException;
import util.DynamicTypingException;
import util.Environment;
import util.ExecutionErrorException;
import util.StaticTypingException;
import util.UndeclaredIdentifierException;
import values.BoolValue;
import values.IValue;
import values.IntValue;

public class ASTGt implements ASTNode{
	ASTNode left, right;
	
	public ASTGt(ASTNode left, ASTNode right) {
		this.left = left;
		this.right = right;
	}
	
	public IValue eval(Environment<IValue> env) throws UndeclaredIdentifierException, DuplicateIdentifierException, ExecutionErrorException, DynamicTypingException {
		IValue l = (IntValue) left.eval(env);
		IValue r = (IntValue) right.eval(env);
		if((l instanceof IntValue && r instanceof IntValue) ){
			return new BoolValue(((IntValue) l).getValue() > ((IntValue) r).getValue());
		} throw new DynamicTypingException(null);
    }

	public String toString() {
		return left.toString() + " > " + right.toString();
	}

	@Override
	public void compile(CodeBlock code, CompilerEnv env) throws UndeclaredIdentifierException, DuplicateIdentifierException {
		left.compile(code, env);
		right.compile(code, env);
		code.emit_gt();	
	}
	
	public IType typeCheck(Environment<IType> env)
			throws UndeclaredIdentifierException, DuplicateIdentifierException, StaticTypingException {
		IType t1 = left.typeCheck(env);
		IType t2 = right.typeCheck(env);
		if(t1==IntType.singleton&&t2==IntType.singleton)
			return BoolType.singleton;
		else
			throw new StaticTypingException("Types dont match!");
	}
}
