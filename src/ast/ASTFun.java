package ast;

import java.util.ArrayList;

import compiler.CodeBlock;
import types.FunType;
import types.IType;
import util.Binding;
import util.CompilerEnv;
import util.DuplicateIdentifierException;
import util.DynamicTypingException;
import util.Environment;
import util.ExecutionErrorException;
import util.StaticTypingException;
import util.TypedBinding;
import util.UndeclaredIdentifierException;
import values.Closure;
import values.IValue;

public class ASTFun implements ASTNode {
	ArrayList<TypedBinding> arguments;
	ASTNode body;

	public ASTFun(ArrayList<TypedBinding> arguments, ASTNode body) {
		this.arguments = arguments;
		this.body = body;
	}

	@Override
	public IValue eval(Environment<IValue> env) throws DynamicTypingException, UndeclaredIdentifierException,
			DuplicateIdentifierException, ExecutionErrorException {
		return new Closure(arguments, body, env);
	}

	@Override
	public IType typeCheck(Environment<IType> env)
			throws UndeclaredIdentifierException, DuplicateIdentifierException, StaticTypingException {		
		IType type;
		IType idType;
		ArrayList<IType> argsType = new ArrayList<IType>();
		Environment<IType> newEnv = env.beginScope();		

		for( TypedBinding arg:arguments){
			newEnv.assoc(arg.getId(), arg.getType());
			argsType.add(arg.getType());
		}

		type = body.typeCheck(newEnv);
//		newEnv.endScope();
	
		return new FunType(argsType,type);
	}

	@Override
	public void compile(CodeBlock code, CompilerEnv env)
			throws UndeclaredIdentifierException, DuplicateIdentifierException {
		// TODO Auto-generated method stub

	}
	
	public String toString() {
		String res = "fun ";
		for(TypedBinding arg:arguments){
			res += arg.getId() + " : " + arg.getType().toString();
			if (!arguments.get( arguments.size()-1).equals(arg)){
				res += ", ";
			}
		}
		return res + " => " + body.toString() + " end";
	}
}
