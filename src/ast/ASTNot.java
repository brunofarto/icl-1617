package ast;

import compiler.CodeBlock;
import types.BoolType;
import types.IType;
import types.IntType;
import util.CompilerEnv;
import util.DuplicateIdentifierException;
import util.DynamicTypingException;
import util.Environment;
import util.ExecutionErrorException;
import util.StaticTypingException;
import util.UndeclaredIdentifierException;
import values.BoolValue;
import values.IValue;

public class ASTNot implements ASTNode {
	ASTNode exp;
	
	public ASTNot(ASTNode exp) {
		this.exp = exp;
	}
	
	public IValue eval(Environment<IValue> env) throws UndeclaredIdentifierException, DuplicateIdentifierException, ExecutionErrorException, DynamicTypingException {
		IValue e = (BoolValue) exp.eval(env);
		if((e instanceof BoolValue) ){
			return new BoolValue(!((BoolValue) e).getValue());
		} throw new DynamicTypingException(null);
    }
	
	public String toString() {
		return "!(" + exp.toString() + ")";
	}

	public void compile(CodeBlock code, CompilerEnv env) throws UndeclaredIdentifierException, DuplicateIdentifierException {
		exp.compile(code, env);
		code.emit_not();
	}
	
	public IType typeCheck(Environment<IType> env)
			throws UndeclaredIdentifierException, DuplicateIdentifierException, StaticTypingException {
		IType e = exp.typeCheck(env);
		if(e==BoolType.singleton)
			return BoolType.singleton;
		else
			throw new StaticTypingException("Types dont match!");
	}

}
