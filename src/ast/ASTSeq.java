package ast;

import compiler.CodeBlock;
import types.IType;
import util.CompilerEnv;
import util.DuplicateIdentifierException;
import util.DynamicTypingException;
import util.Environment;
import util.ExecutionErrorException;
import util.StaticTypingException;
import util.UndeclaredIdentifierException;
import values.IValue;

public class ASTSeq implements ASTNode {
	ASTNode left, right;
	
	public ASTSeq(ASTNode left, ASTNode right){
		this.left = left;
		this.right = right;
	}

	@Override
	public IValue eval(Environment<IValue> env) throws DynamicTypingException, UndeclaredIdentifierException,
			DuplicateIdentifierException, ExecutionErrorException {
		left.eval(env);
		return right.eval(env);
	}

	@Override
	public IType typeCheck(Environment<IType> env)
			throws UndeclaredIdentifierException, DuplicateIdentifierException, StaticTypingException {
		left.typeCheck(env);
		return right.typeCheck(env);
	}

	@Override
	public void compile(CodeBlock code, CompilerEnv env) throws UndeclaredIdentifierException, DuplicateIdentifierException {
		left.compile(code,env);
		code.emit_pop();
		right.compile(code,env);
	}
	
    public String toString() {
    	return left.toString() + " ; " + right.toString();
    }

}
