package values;

public class BoolValue implements IValue {
	boolean val;
	
	public BoolValue(boolean val) {
		this.val = val;
	}
	
	public boolean getValue() {
		return val;
	}
	
	public String toString() {
		return Boolean.toString(val);
	}
	
	public boolean equals(Object other) {
		return (other instanceof BoolValue) && ((BoolValue) other).val == this.val; 
	}
}
