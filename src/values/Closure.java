package values;

import java.util.ArrayList;

import ast.ASTNode;
import util.Environment;
import util.TypedBinding;

public class Closure implements IValue{
	ArrayList<TypedBinding> arguments;
	private ASTNode exp;
	private Environment<IValue> env;
	
	public Closure(ArrayList<TypedBinding> arguments, ASTNode exp, Environment<IValue> env){
		this.arguments = arguments;
		this.env = env;
		this.exp = exp;
	}

	public ArrayList<TypedBinding> getArguments() {
		return arguments;
	}

	public ASTNode getExp() {
		return exp;
	}

	public Environment<IValue> getEnv() {
		return env;
	}
}
