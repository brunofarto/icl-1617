package values;

public class RefValue implements IValue {
	IValue val;
	
	public RefValue(IValue val) {
		this.val = val;
	}

	
	public String toString() {
		return val.toString();
	}

	public IValue getValue() {
		return val;
	}

	public void setVal(IValue val) {
		this.val = val;
	}
	
	public boolean equals(Object other) {
		return this == other; 
	}
}
