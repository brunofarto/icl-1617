package values;

public class IntValue implements IValue {
	int val;
	
	public IntValue(int val) {
		this.val = val;
	}
	
	public int getValue() {
		return val;
	}
	
	public String toString() {
		return Integer.toString(val);
	}
	
	public boolean equals(Object other) {
		return (other instanceof IntValue) && ((IntValue) other).val == this.val; 
	}
}
