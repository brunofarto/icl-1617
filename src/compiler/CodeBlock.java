package compiler;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;

import ast.ASTNode;
import compiler.CodeBlock.StackFrame;
import types.IType;
import types.RefType;
import util.Binding;
import util.CompilerEnv;
import util.DuplicateIdentifierException;
import util.UndeclaredIdentifierException;

public class CodeBlock {
	public interface IFrame {
		void dump();
	}
	
	public static class StackFrame implements IFrame{
		int number;
		ArrayList<Binding> decls;
		StackFrame parentFrame;
		static int fnumber = 0;
		
		StackFrame(ArrayList<Binding> decls, StackFrame parentFrame) {
			this.number = fnumber++;
			this.decls = decls;
			this.parentFrame = parentFrame;
		}
		
		public int getNumber(){
			return number;
		}
		
		public String getType() {
			return "frame_"+number;
		}
		
		public void dump() {
			// open file�  
			String frameName = "frame_"+number;
	    	File file = new File(frameName+".j");
//	    	files.push(file);

			BufferedWriter bw;
			
			// write header
			try {
				bw = new BufferedWriter(new FileWriter(file));
	   	
				bw.write(".source "+frameName+".j");
				bw.newLine();
				bw.write(".class "+frameName);
				bw.newLine();
				bw.write(".super java/lang/Object");
				bw.newLine();
				bw.write(".implements frame");
				bw.newLine();
				bw.newLine();
				if( this.parentFrame.number != 0 ){
					bw.write(".field public SL Lframe_"+this.parentFrame.number+";");
					bw.newLine();
					bw.newLine();
				}		
				// write ndecls field declarations
				int i = 0;
				for(Binding id:decls){
					String type = type(id.getType());
					bw.write(".field public loc_"+i+" "+(type.equals("I")?type:type+";"));
					bw.newLine();
					i++;
				}

				// write footer
				bw.newLine();
				bw.write(".method public <init>()V");
				bw.newLine();
				bw.write("aload_0");
				bw.newLine();
				bw.write("invokespecial java/lang/Object/<init>()V");
				bw.newLine();
				bw.write("return");
				bw.newLine();
				bw.write(".end method");
				bw.flush();
	        	bw.close();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
	}
	
	public static class TypeFrame implements IFrame {
		private String type;
		
		
		public TypeFrame(String type){
			this.type = type;
		}
		
		public String getType(){
			return type;
		}

		@Override
		public void dump() {
			// TODO Auto-generated method stub			// open file�  
	    	File file = new File(type+".j");

			BufferedWriter bw;
			
			// write header
			if (!file.exists()) {
				try {
					bw = new BufferedWriter(new FileWriter(file));
		   	
					bw.write(".source "+type+".j");
					bw.newLine();
					bw.write(".class "+type);
					bw.newLine();
					bw.write(".super java/lang/Object");
					bw.newLine();
					bw.newLine();
					
					if(type=="ref_int")
						bw.write(".field public value I");
					else
						bw.write(".field public value Lref_int;");
	
					// write footer
					bw.newLine();
					bw.newLine();
					bw.write(".method public <init>()V");
					bw.newLine();
					bw.write("aload_0");
					bw.newLine();
					bw.write("invokespecial java/lang/Object/<init>()V");
					bw.newLine();
					bw.write("return");
					bw.newLine();
					bw.write(".end method");
					bw.flush();
		        	bw.close();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		}
		
	}
	
	private static final String SP = "0";
	
	ArrayList<String> code;
	ArrayList<IFrame> frames;
	StackFrame currentFrame;
	int labelcounter=0;

	public CodeBlock() {
		code = new ArrayList<String>(100);
		frames = new ArrayList<IFrame>();
		currentFrame = new StackFrame(null,null);

//        demo = new File("Demo.j");
//        dumpHeader();
		// if file doesnt exists, then create it
        File file = new File("frame.j");
		if (!file.exists()) {
			try {
				file.createNewFile();
				FileWriter fw = new FileWriter(file.getAbsoluteFile());
				BufferedWriter bw = new BufferedWriter(fw);
				bw.write(".source frame.j");
				bw.newLine();
				bw.write(".interface public frame");
				bw.newLine();
				bw.write(".super java/lang/Object");
				bw.newLine();	
				bw.flush();
				bw.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
	
			
		}
	}
	
	public StackFrame getCurrentFrame(){
		return currentFrame;
	}

	public StackFrame createFrame(ArrayList<Binding> decls) {
		StackFrame frame = new StackFrame(decls, currentFrame);
		frames.add(frame);
		return frame;
	}
	
	public TypeFrame createRefFrame(IType refType) {
		TypeFrame typeFrame = new TypeFrame(refClassOfType(refType));
		frames.add(typeFrame);
		return typeFrame;
	}

	public void initFrame(StackFrame frame){
		code.add("       new frame_"+frame.number+" ; create a stack frame");
		emit_dup();
		code.add("       invokespecial frame_"+frame.number+"/<init>()V");
		if(frame.parentFrame.number!=0){
			emit_dup();
			aload_0();
			code.add("       putfield frame_"+frame.number+"/SL Lframe_"+frame.parentFrame.number+";");
		}
	}
	
	public void initTypeFrame(TypeFrame frame){
		code.add("       new "+frame.type+" ; create a stack frame");
		emit_dup();
		code.add("       invokespecial "+frame.type+"/<init>()V");
		emit_dup();
	}

	private String reftypeToJasmin(String type) {
		return "L"+type+";";
	}
	
    public static String type(IType type){
    	if(type instanceof RefType )
    		if(((RefType) type).getType()instanceof RefType)
    			return "Lref_class";
    		else
    			return "Lref_int";
    	else
    		return "I";
    }
    
    public static String refClassOfType(IType type){
    	if(type instanceof RefType )
    		if(((RefType) type).getType()instanceof RefType)
    			return "ref_class";
    		else
    			return "ref_int";
    	else
    		return "int";
    }
	
	public void emit_storeStack(StackFrame nframe, int offSet, String type) {
		code.add("       putfield frame_"+nframe.number+"/loc_"+offSet+" "+(type.equals("I")?type:type+";"));
	}
	
	private String createLabel(){
		return "L"+labelcounter++;
	}

	public void emit_pushFrame(StackFrame frame) {
		astore_0();
		currentFrame = frame;
	}
	
	public void emite_popFrame() {
		if(currentFrame.parentFrame.number != 0){
			aload_0();
		    code.add("       checkcast "+currentFrame.getType());
		    code.add("       getfield "+
		    		   	    currentFrame.getType()+"/SL "+
		    		   	    this.reftypeToJasmin(currentFrame.parentFrame.getType()));
			astore_0();
		} else {
			code.add("       aconst_null ; cleans the top of the stack");
			code.add("       astore 0");
		}
		currentFrame = currentFrame.parentFrame;
	}
	
    public void emitPutfield(String r,String v, String type){
    	 code.add("       putfield "+r+"/"+v+" "+type);
    }
    
	public void emitGetfield(String r, String v, String type) {
		code.add("       getfield "+r+"/"+v+" "+type);
	}
    
    public void checkcast(String framename){
    	code.add("       checkcast "+framename);
    }
    
    public void dup(){
		code.add("       dup");
    }
	
    public void comment(String comment){
    	code.add("       ;"+comment);
    }
	
    public void emitIf(ASTNode e1, ASTNode e2,CodeBlock c, CompilerEnv env) throws UndeclaredIdentifierException, DuplicateIdentifierException {
		String label1 = createLabel();
		String label2 = createLabel();

		code.add("       ifeq label_"+label1);
		e1.compile(c, env);
		code.add("       goto label_"+label2);
		code.add("       label_"+label1+":");
		e2.compile(c, env);
		code.add("       label_"+label2+":");
	}
    
	public void emitWhile(ASTNode c, ASTNode e, CodeBlock code2, CompilerEnv env) throws UndeclaredIdentifierException, DuplicateIdentifierException  {
		String label1 = createLabel();
		String label2 = createLabel();

    	code.add("       label_"+label2+":");
    	c.compile(code2, env);  	
    	code.add("       ifeq label_"+label1);
    	e.compile(code2, env);
    	emit_pop();
    	code.add("       goto label_"+label2);    	
    	code.add("       label_"+label1+":");
    	emit_push(0);
	}
	
	public void emit_dup(){
		code.add("       dup					; save the reference for later");
	}
	
	public void emit_pop(){
		code.add("       pop");
	}
	
    public void astore_0(){
    	code.add("       astore 0");
    }
    
    public void aload_0(){
    	code.add("       aload 0");
    }
    
    public void getfield(String field){
    	code.add("       getfield "+field);
    }
    
	public void checkcast() {
		code.add("       checkcast frame_"+currentFrame.number);
	}

	public void emit_push(int n) {
		code.add("       sipush "+n);
	}

	public void emit_add() {
		code.add("       iadd");
	}

	public void emit_mul() {
		code.add("       imul");
	}

	public void emit_div() {
		code.add("       idiv");
	}

	public void emit_sub() {
		code.add("       isub");
	}
	
	public void emit_and() {
		code.add("       iand");
	}

	public void emit_or() {
		code.add("       ior");
	}
	
	public void emit_not() {
		emit_push(1);
		code.add("       ixor");
	}
	
	public void emit_eq() {
		String label1 = createLabel();
		String label2 = createLabel();
		code.add("       if_icmpne "+label1);
		emit_push(1);
		code.add("       goto "+label2);
		code.add("       "+label1+":");
		emit_push(0);
		code.add("       "+label2+":");
	}
	
	public void emit_neq() {
		String label1 = createLabel();
		String label2 = createLabel();
		code.add("       if_icmpeq "+label1);
		emit_push(1);
		code.add("       goto "+label2);
		code.add("       "+label1+":");
		emit_push(0);
		code.add("       "+label2+":");
	}
	
	public void emit_ge() {
		String label1 = createLabel();
		String label2 = createLabel();
		code.add("       if_icmplt "+label1);
		emit_push(1);
		code.add("       goto "+label2);
		code.add("       "+label1+":");
		emit_push(0);
		code.add("       "+label2+":");
	}
	
	public void emit_gt() {
		String label1 = createLabel();
		String label2 = createLabel();
		code.add("       if_icmple "+label1);
		emit_push(1);
		code.add("       goto "+label2);
		code.add("       "+label1+":");
		emit_push(0);
		code.add("       "+label2+":");
	}
	
	public void emit_le() {
		String label1 = createLabel();
		String label2 = createLabel();
		code.add("       if_icmpgt "+label1);
		emit_push(1);
		code.add("       goto "+label2);
		code.add("       "+label1+":");
		emit_push(0);
		code.add("       "+label2+":");
	}
	
	public void emit_lt() {
		String label1 = createLabel();
		String label2 = createLabel();
		code.add("       if_icmpge "+label1);
		emit_push(1);
		code.add("       goto "+label2);
		code.add("       "+label1+":");
		emit_push(0);
		code.add("       "+label2+":");
	}

	void dumpHeader(PrintStream out) {
		out.println(".class public Demo");
		out.println(".super java/lang/Object");
		out.println("");
		out.println(";");
		out.println("; standard initializer");
		out.println(".method public <init>()V");
		out.println("   aload_0");
		out.println("   invokenonvirtual java/lang/Object/<init>()V");
		out.println("   return");
		out.println(".end method");
		out.println("");
		out.println(".method public static main([Ljava/lang/String;)V");
		out.println("       ; set limits used by this method");
		out.println("       .limit locals 10");
		out.println("       .limit stack 256");
		out.println("");
		out.println("       ; setup local variables:");
		out.println("");
		out.println("       ;    1 - the PrintStream object held in java.lang.out");
		out.println("       getstatic java/lang/System/out Ljava/io/PrintStream;");
		out.println("");
		out.println("       ; place your bytecodes here");
		out.println("       ; START");
		out.println("");
	}

	void dumpFooter(PrintStream out) {
		out.println("       ; END");
		out.println("");
		out.println("");		
		out.println("       ; convert to String;");
		out.println("       invokestatic java/lang/String/valueOf(I)Ljava/lang/String;");
		out.println("       ; call println ");
		out.println("       invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V");
		out.println("");		
		out.println("       return");
		out.println("");		
		out.println(".end method");
	}

	void dumpCode(PrintStream out) {
		for( String s : code )
			out.println("       "+s);
	}

	private void dumpFrames() {
	for(IFrame f: frames) 
		f.dump();
	}
	
	
	public void dump(String filename) throws FileNotFoundException {
		dumpFrames();
		
		PrintStream out = new PrintStream(new FileOutputStream(filename));
		dumpHeader(out);
		dumpCode(out);
		dumpFooter(out);
	}
}